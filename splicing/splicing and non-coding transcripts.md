# Summary

Our current model, where a transcript has a splicing field containing multiple splicing events:

```
transcript -> splicing -> product
           -> splicing -> product
```

works fairly well when describing transcripts adhering to the central dogma:

```
transcript -> mRNA -> protein
```

But we struggle to fit other biological processes into it, for example:

- precursor RNA splitting into multiple smaller RNAs, as happens with microRNAs:
```
precursor RNA -> miRNA
              -> miRNA
```

- viral gene producing a large precursor protein that is then split into smaller proteins (as is the case with SARS-CoV-2):
```
viral RNA -> precursor protein -> protein
                               -> protein
```

There are several possible directions we can take:

## 1. Keep trying to fit coding transcripts and all the other transcripts into the same model.

### Advantages:
- it will be a single universal model

### Disadvantages:
- a number of fields will have to become nullable
- it may become a challenge to find names for fields (for example, the name "splicing" that we are currently using goes beyond the biological meaning of the word "splicing" as stitching of different pieces of gene together, and thus causes a fair amount of rejection)

## 2. Add special fields to the gene describing different kinds of transcripts

```
{
  gene {
    proteinCodingTranscripts {
      ...
    },
    miRNATranscripts {
      ...
    },
    lncRNATranscripts {
      ...
    },
  }
}

```

### Advantages:
- each field will contain transcripts only of certain type; therefore they can contain different sets of fields

### Disadvantages:
- cumbersome gene model, which will have multiple fields for multiple types of transcripts

## 3. Model transcript as a union of different types of transcripts
Borrowing the concept of types from GraphQL, we would accept that Transcript is in fact a union of different types of transcripts, each having its own set of fields. The gene model will then only need the `transcripts` field.

### Advantages:
- clean gene model

### Disadvantages:
- consumers of Thoas will have to use somewhat more advanced querying syntax to resolve type unions:

```
{
  transcript {
    ...on ProteinCodingTrandscript {
      splicing {

      }
    }
    ... miRNATranscript {

    }
  }
}
```

# Other topics discussed during the meeting
- Features whose sequences can be described as a simple slice of genomic sequence (e.g. genes and transcripts) will not have a sequence field. To get their sequence, user will have to query refget with:
  - hash of the region (e.g. hash for the sequence of human chromosome 1)
  - start coordinate
  - end coordinate
- In contrast, non-features such as CDSs or cDNAs will have their own sequence and sequence_checksum fields


# Attempts to model the contents of the splicing container for some non-coding transcripts

## lncRNA
splicing container:
```js
{
  "product_type": null,
  "default": true,
  "cds": null,
  "product": null,
  "spliced_exons": [
      {
      "start_phase": null,
      "end_phase": null,
      "index": 0,
      "exon": {
        "stable_id": "CAK10225-1",
        "type": "exon",
        "relative_slice": {
          "location": {
            "start": 1,
            "end": 1032,
            "length": 1032
          }
        },
        "slice": {
          "location": {
            "start": 5056183,
            "end": 72,
            "length": 1032
          },
          "region": {
            "name": "Chromosome",
            "code": "chromosome",
            "strand": {
              "code": "reverse",
              "value": -1
            }
          }
        }
      }
    }
  ]
}
```

## miRNA
Notice that there may be some "interesting features" in the precursor RNA, which will have to be described in a field at the transcript level.

```js
  transcript: {
    interesting_features: ...,
    splicing: [
      {
        "product_type": "miRNA",
        "default": true,
        "cds": null,
        "product": {
          "stable_id": "blah1",
          "type": "miRNA",
          "length": 343,
          "splice_junction_positions": null,
          "sequence": ... 
        }
      },
      {
        "product_type": "miRNA",
        "default": true,
        "cds": null,
        "product": {
          "stable_id": "blah2",
          "type": "miRNA",
          "length": 343,
          "splice_junction_positions": null,
          "sequence": ... 
        }
      },
    ]
  }

```





