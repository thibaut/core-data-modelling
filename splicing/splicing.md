# Rationale
1. Historically, in Ensembl, a transcript has been modelled to have more than one product (and more than one CDS). This is not even to account for alternative splicing (Ensembl represents alternative splicing events as different transcripts), but to be able to represent polycystronic genes.
2. Because of (1), we need a mechanism to associate exons with their corresponding CDSs and products
3. A shallow model, in which exons, CDSs and products have ids of each other (as foreign keys) is insufficient, because each exon will have a phase, which exists only in the context of a splicing event.

Therefore, we are proposing a `splicing` field that is a list of all splicing events, associating exons, products and CDSs with each other (see example.json).


# Example

```js
"splicing": [
  {
    "product_type": "protein",
    "default": true,
    "cds": {
      "start": 32316461,
      "end": 32316527,
      "relative_start": 390,
      "relative_end": 456,
      "protein_length": 22,
      "nucleotide_length": 67
    },
    "product": {
      "stable_id": "ENSP00000.1",
      "type": "protein",
      "length": 22,
      "splice_junction_positions": [11, 18],
      "sequence": "MPIGSKERPTFFEIFKTRCNKA",
      "sequence_checksum": "truncatedsha512",
      "external_references": [
        {
          "accession_id": "R-HSA-5693532",
          "name": "R-HSA-5693532",
          "description": "DNA Double-Strand Break Repair",
          "source": {
            "name": "Reactome Pathway",
            "url": "https://reactome.org"
          }
        }
      ],
      "protein_domains": []
    },
    "spliced_exons": [
      {
        "start_phase": -1,
        "end_phase": 1,
        "exon": {
          "stable_id": "ENSE...",
          ... rest of the regular exon fields
        }
      }
    ],
    "spliced_introns": [
      {
        "so_term": "blah",
        "slice": Slice,
        order: 1
      },
      {
        so_term: blah,
        slice: Slice,
        order: 2
      }
    ]
  }
]
```

# Comments on the example
- The `splicing` field is a list that consists of one or many "splicing events".
- One of the "splicing events" is marked as default, for the client to know what to draw
- A CDS makes sense only in the context of a "splicing event". It is, therefore, not a feature in itself, and should not be required to have fields common with the rest of features.
- Each exon participating in a "splicing event" is placed in the `spliced_exons` container. Each exon contains regular exon fields, plus the `start_phase` and the `end_phase` fields, which only make sence in the context of a splicing event.


# Comments regarding the order of exons in spliced_exons field
- We (Kieron and Andrey) feel that we do not like a dedicated `order`/`rank`/`index` field on spliced exon objects and would prefer to rely on the order of objects inside of the array
- Exons should be ordered in the way they are spliced (i.e. relative to transcript rather than to the chromosome, because on reverse strand their spliced order will be opposite to their order on the chromosome).

For example, given two strands of DNA with the exons located on the reverse strand:

```
5' forward strand: ----------------- 3'
3' reverse strand  ----ex1-ex2-ex3-- 5'
```

we want the order of the transcripts to be `[ex3, ex2, ex1]`.

# Problems
We still do not know how to model relationships of a transcript to products if this relationship is not described in terms of splicing. Andy gave an example of the coronavirus that produces a precursor protein (Orf1ab) which then gets cleaved into 16 other proteins. How do we even approach this?

We are also not sure the above grouping will help in any way with polycystronic genes.

# Other directions to explore
Since Ensembl is not trying to fit alternative splicing data into a single transcript, most of Ensembl transcripts have a one-to-one relationship with their products (at least that's what the website has always assumed). Perhaps we should consider different types of transcripts: e.g. classical transcript, polycistronic transcript, viral transcript, etc., which will have different relationships between the transcript and its product(s).

# Questions:

- Do we want the api to include introns in its response?

(Variation team says they are interested in variants inside introns near the exon-intron boundary. They are also interested in having intron sequences. We expect groups outside Ensembl will be interested in same.)
