# Transcript  BRCA2-210

## View in Ensembl:
https://www.ensembl.org/Homo_sapiens/Transcript/Summary?db=core;g=ENSG00000139618;r=13:32315086-32400266;t=ENST00000670614

## Data from Rest:
https://rest.ensembl.org/lookup/id/ENST00000670614?content-type=application/json;expand=1
